#include <unordered_map>
#include <iostream>

void apply(std::unordered_map<char const *, int> & map);

void apply(std::unordered_map<char const *, int> & map) {
	std::cout << map.at("test") << "\n";
}

int main() {
	std::unordered_map<char const*, int> map;
	map["test"] = 1;
	apply(map);
}
